var data_import = [];

var path_data           = '';
var width_svg           = 600;
var height_svg          = 600;
var color_svg           = "rgba(255,255,255,0.2)";
var colours             = ["CornflowerBlue", "Crimson", "DarkGreen", "DarkKhaki", "DarkOrange", "Gold", "GreenYellow", "HotPink"];
var show_data           = true;
var width_col           = 20;
var columns_svg         = true;
var color_selected      = "yellow";

//Init Template.
/*
* You can send empty param. So app get config to config.json.
* You can send this params:
*     initiate(   <path_data>,                  --> String with path of file JSON with datas
*                 <width_svg>,                  --> Number with width of panel SVG
*                 <height_svg>,                 --> Number with height of panel SVG
*                 <color_svg>                   --> String with background-color of panel SVG
*                 <colours>,                    --> Array with colours for bars
*                 <show_data>,                  --> Boolean to show values in each bar
*                 <width_col>                   --> Number with width of columns. Only accept if columns_svg is false.
*                 <columns_svg>                 --> Boolean to width of columns calculated with width of SVG.
*                 <color_selected>)             --> Color to paint bar when it is selected.
*/
function initiateBars(likes, selector){

      //Load data
      load_data_bars(likes, selector);
};

//load data.
function load_data_bars(likes, selector){
      var data = [];

      for (index in likes){
            data.push({"info":likes[index].title, "data":likes[index].likes});
      }
      
      data_import = data;

      //Draw Bar Graphic
      draw_graphic_bars(selector);
}

//Draw Bar Graphic
function draw_graphic_bars(selector){

      //Adapt width_col to width_svg.
      if (columns_svg){
            //width_col = (width_svg - margin-left - one pixel for bar - margin-right) / numbers of bars
            width_col = (width_svg - 30 - (data_import.length) - 30)/data_import.length;
      }

      //Scales
      var yRange = d3.scaleLinear()
                     .domain([d3.max(data_import, function(d){
                           return d.data;
                     }),0])
                     .range([0, (height_svg-20)]);
      
      var yRangeData = d3.scaleLinear()
                         .domain([0, d3.max(data_import, function(d){
                              return d.data;
                         })])
                         .range([0, (height_svg-20)]);

      var xRange = d3.scaleLinear()
                     .range([0, width_svg - 60 - 1])
                     .domain([0, data_import.length]);

      //Init SVG
      var svg = d3.select(selector)
                  .append("svg")
                  .attr("width", width_svg+250)
                  .attr("height", height_svg+50)
                  .style("background-color", color_svg);
    
      svg.append("g")
         .attr("class", "axis axis--x")
         .attr("transform", "translate(30," + (height_svg - 5) + ")")
         .call(d3.axisBottom(xRange));


      svg.append("g")
         .attr("class", "axis axis--y")
         .attr("transform", "translate(30,15)")
         .call(d3.axisLeft(yRange));
                
      //Load bars
      svg.selectAll("rect")
            .data(data_import)
            .enter()
            .append("rect")
            .attr("x", function(d,i){
                  return i * (width_col + 1) + 31;
            })
            .attr("y", function(d){
                  return height_svg - yRangeData(d.data) - 5;
            })
            .attr("width", width_col)
            .attr("height", function(d){
                  return yRangeData(d.data);
            })
            .attr("fill", "CornflowerBlue")
            .on("mouseover", function(d, i){
                  d3.select(this)
                  .attr("fill", color_selected);
            })
            .on("mouseout", function(d, i){
                  d3.select(this)
                  .attr("fill", "CornflowerBlue");
            });
      
      svg.selectAll("label")
            .data(data_import)
            .enter()
            .append("text")
            .attr("font-size", function(){return (width_col*0.20) + "px"})
            .attr("color", "black")
            .text(function(d){ return d.info})
            .attr("x", function(d,i){
                  return i * (width_col + 1) + 33;
            })
            .attr("y", function(d){
                  return height_svg - yRangeData(d.data) + 7 + (width_col*0.20);
            })
            .attr("width", width_col)
            .attr("height", function(d){
                  return yRangeData(d.data);
            });
};
