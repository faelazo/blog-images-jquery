/******************************************************************************
*     This file contain the logic of Header: build menu, clicks on
*     options, etc.
*
*******************************************************************************/

//Build options of menu.
function getMenu(selector){
      $(selector).html(()=>{
            var options = "";

            for (option in MENU){
                  var click = 'selectedOption("' + MENU[option] + '")';
                  options += "<li class='menuOption' onClick='" + click + "'>" + MENU[option].toUpperCase() + "</li>"
            }

            return options;
      });
};

//Handle event onClick of options of menu.
function selectedOption(option){

      for (index in MENU){
            $("#div"+MENU[index].charAt(0).toUpperCase() + MENU[index].substr(1)).hide();
      }

      $("#div"+option.charAt(0).toUpperCase() + option.substr(1)).show();
};
