
function getStatistics(images, posts){

      loadPie(images, posts);

      loadBars(images, posts);

      showPie();

}

function insertPanels(){
      var html = "";

      html += "<div id='selector'>"
      html += "   <h3>Seleccionar:</h3>";
      html += "   <form action=''>";
      html += "         <input type='radio' name='selectGraphic' value='pie' onchange='showPie();'' checked>Tarta<br>";
      html += "         <input type='radio' name='selectGraphic' value='bar' onchange='showBars();'>Barras<br>";
      html += "   </form>"
      html += "</div>"

      //Likes to images
      html += "<h1>Graphic Images</h1>";
      html += "<div id='graphicImages'>";
      html += "   <div id='pieImages'>";
      html += "   </div>";
      html += "   <div id='barImages'>";
      html += "   </div>";
      html += "</div>";

      //Likes to posts
      html += "<h1>Graphic Posts</h1>";
      html += "<div id='graphicPosts'>";
      html += "   <div id='piePosts'>";
      html += "   </div>";
      html += "   <div id='barPosts'>";
      html += "   </div>";
      html += "</div>";

      return html;
}

function loadPie(images, posts){
      initiatePie(images, "#pieImages");

      initiatePie(posts, "#piePosts");
}

function loadBars(images, posts){
      initiateBars(images, "#barImages");

      initiateBars(posts, "#barPosts");
}

function showPie(){
      $("#pieImages").show();
      $("#piePosts").show();
      $("#barImages").hide();
      $("#barPosts").hide();
}

function showBars(){
      $("#pieImages").hide();
      $("#piePosts").hide();
      $("#barImages").show();
      $("#barPosts").show();
}
