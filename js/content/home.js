
function getHome(images, posts){
      var html = "";

      var data = images.concat(posts)
                       .sort((a, b)=> {
                              return Date.parse(a.datePublic) - Date.parse(b.datePublic)
                        })

      for (index in data){
            if (data[index].type == "IMAGE"){

                html += getImagePostMin(data[index]);

            }else if (data[index].type == "POST"){

                html += getPostMin(data[index]);
            }
      }

      return html;
}
