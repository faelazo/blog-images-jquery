//When document is ready, all elements of DOM are loaded. So we can start...
$(document).ready(function(){

      /****** VARIABLES *******/
      var images = [];
      var posts = [];

      /****** BUILDING PAGE ******/
      //Build menu
      $("#menu ul").html(getMenu("#menu ul"));

      //Build sections
      $("#divContent").html(getContent("#divContent"));

      //Load data JSONs
      $.ajax({
          dataType: 'json',
          url: './data/images.json',
          success: function(data){
              //Load data images
              images = data;

              $.ajax({
                  dataType: 'json',
                  url: './data/posts.json',
                  success: function(data){
                      //Load data posts
                      posts = data;

                      //Build home
                      $("#divHome").html(getHome(images, posts));

                      //Build blog
                      $("#divBlog").html(getBlog(posts));

                      //Build images
                      $("#divImages").html(getImages(images));

                      //Build statistics
                      $("#divStatistics").html(insertPanels());
                      $("#divStatistics").html(getStatistics(images, posts));

                  }
              });
          }
      });

      //Build about
      $("#divAbout").html(getAbout());

      /****** INITIALIZE PAGE ******/
      selectedOption(MENU[0]);

      /****** HANDLER SECTION POST AND IMAGE ******/
      function openImagePost(id){

            var image;
            for (index in images){
                if (images[index].id == id){
                  image = images[index];
                }
            };

            $("#contentPanel").html(getImagePost(image));
            $("#contentPanel").show();
      };

      function openPost(id){
            var post;
            for (index in posts){
                if (posts[index].id == id){
                  post = posts[index];
                }
            };

            $("#contentPanel").html(getPost(post));
            $("#contentPanel").show();
      };

      function getContent(selector){
            $(selector).html(()=>{
                  var options = "";
      
                  for (option in MENU){
                        options += "<div id='div" + MENU[option].charAt(0).toUpperCase() + MENU[option].substr(1) + "' class='divContentMain'></div>"
                  }
      
                  return options;
            });
      };

      $("#contentPanel").on('click', '.btStar0', function(){
            $(this).attr('class', 'btStar1');
      });

      $("#contentPanel").on('click', '.btStar1', function(){
            $(this).attr('class', 'btStar0');
      });

      $("#divContent").on('click', '.imagePostMinClass', function(){
            var idImage = $(this).attr('id');
            var id = idImage.substr("imageMin".length);

            openImagePost(id);
      });

      $("#divContent").on('click', '.listImagePostClass', function(){
            var idImage = $(this).attr('id');
            var id = idImage.substr("imageList".length);

            openImagePost(id);
      });

      $("#divContent").on('click', '.postMinClass', function(){
            var idPost = $(this).attr('id');
            var id = idPost.substr("postMin".length);

            openPost(id);
      });

      $("#divContent").on('click', '.listPostClass', function(){
            var idPost = $(this).attr('id');
            var id = idPost.substr("postList".length);

            openPost(id);
      });

      $("#contentPanel").on('click', '.divExit h1', function(){
            $("#contentPanel").hide();
      });
});
