/******************************************************************************
*     This file contain the logic of post..
*
*******************************************************************************/

//Return post-min structure HTML for home panel
function getPostMin(post){
      var html = "";

      html =  "<div id='postMin" + post.id + "' class='postMinClass' >";
      html += "    <h1 class='h1PostMin'>" + post.title.toUpperCase() + "</h1>"
      html += "    <p class='pDatePubli'>Publicado el " + post.datePublic + "</p>"
      html += "    <section class='sectionPostMin'>"
      html += "        <p class='pPostMin'>" + post.content + "</p>"
      html += "    </section>"
      html += "</div>"

      return html;
}

//Return post structure HTML for list of post in Blog panel
function getListPost(post){
    var html = "";

    html += "<div id='postList" + post.id + "' class='listPostClass'>"
    html += "    <h1 class='h1Post'>" + post.title.toUpperCase() + "</h1>"
    html += "    <p class='pDatePubli'>Published at " + post.datePublic + "</p>"
    html += "    <section class='sectionPost'>"
    html += "        <p>" + post.content + "</p>"
    html += "    </section>"
    html += "    <h3 class='h3Post'>Read more</h3>"
    html += "</div>"

    return html;
}

//Return post stucture to post panel
function getPost(post){
    var html = "";

    html += "<div id='post" + post.id + "' class='postClass'>";
    html += "    <div class='divExit'>";
    html += "        <h1>X</h1>";
    html += "    </div>";
    html += "    <div class='divExif'>";
    html += "        <h3>" + post.title.toUpperCase() + "</h3>";
    html += "        <button class='btStar0'></button>"
    html += "        <h3>Description</h3>";
    html += "        <p>" + post.content + "</p>";
    html += "    </div>";
    html += "</div>";

    return html;
}
