/******************************************************************************
*     This file contain the logic of image-post.
*
*******************************************************************************/

//Return image-post-min structure HTML
function getImagePostMin(image){
      var html = "";
      html =  "<div id='imageMin" + image.id + "' class='imagePostMinClass' >";
      html += "    <h1 class='h1PostMin'>" + image.title + "</h1>"
      html += "    <p class='pDatePubli'>Publicado el " + image.datePublic + "</p>"
      html += "    <img class='imgPostMin' src='" + image.pageURL + "'></img>"
      html += "    <section class='sectionPostMin'>"
      html += "        <p class='pPostMin'>" + image.description + "</p>"
      html += "    </section>"
      html += "</div>"

      return html;
}

//Return structure HTML for image post to list it in Image Panel
function getListImagePost(image){
      var html = "";

      html += "<div id='imageList" + image.id + "' class='listImagePostClass'>";
      html += "    <h1 class='h1Post'>" + image.title + "</h1>";
      html += "    <p class='pDatePubli'>Published at " + image.datePublic + "</p>";
      html += "    <img class='imgPost' src='" + image.pageURL + "' alt='Málaga' ></img>";
      html += "    <section class='sectionPost'>";
      html += "        <p class='pImagePost'>" + image.description + "</p>";
      html += "    </section>";
      html += "</div>";

      return html;
}

//Return image panel structure HTML
function getImagePost(image){
      var html = "";

      html += "<div id='image" + image.id + "' class='imagePostClass'>";
      html += "    <div class='divImage'>";
      html += "        <img class='imgMaxPost' src='" + image.pageURL + "' alt='Málaga' ></img>";
      html += "    </div>";
      html += "    <div class='divExit'>";
      html += "        <h1>X</h1>";
      html += "    </div>";
      html += "    <div class='divExif'>";
      html += "        <h3>" + image.title + "</h3>";
      html += "        <button class='btStar0'></button>"
      html += "        <ul>";
      html += "            <li><p>Speed = " + image.speed + "</p></li>";
      html += "            <li><p>Aperture = " + image.aperture + "</p></li>";
      html += "            <li><p>ISO = " + image.iso + "</p></li>";
      html += "        </ul>";
      html += "        <h3>Description</h3>";
      html += "        <p>" + image.description + "</p>";
      html += "    </div>";
      html += "</div>";

      return html;
}
